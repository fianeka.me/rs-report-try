  $(function(){
    'use strict'
    // var ctx1 = document.getElementById('chartBar1').getContext('2d');
    /** PIE CHART Tipe **/
    var label = grap2[0];
    var data = grap2[1];
    var color = grap2[2];
    var datapie = {
      labels: label,
      datasets: [{
        data: data,
        backgroundColor: color
      }]
    };

    var optionpie = {
      maintainAspectRatio: false,
      responsive: true,
      legend: {
        display: false,
      },
      animation: {
        animateScale: true,
        animateRotate: true
      }
    };

    // For a pie chart
    var ctx1 = document.getElementById('chartUrji');
    var myDonutChart = new Chart(ctx1, {
      type: 'doughnut',
      data: datapie,
      options: optionpie
    });

    var label = grap3[0];
    var data = grap3[1];
    var color = grap3[2];

    var datapie = {
      labels: label,
      datasets: [{
        data: data,
        backgroundColor: color
      }]
    };

    // For a pie chart
    var ctx2 = document.getElementById('chartMetod');
    var myDonutChart = new Chart(ctx2, {
      type: 'doughnut',
      data: datapie,
      options: optionpie
    });

    var label = grap4[0];
    var data = grap4[1];
    var color = grap4[2];

    var datapie = {
      labels: label,
      datasets: [{
        data: data,
        backgroundColor: color
      }]
    };

    // For a pie chart
    var ctx3 = document.getElementById('chartCara');
    var myDonutChart = new Chart(ctx3, {
      type: 'doughnut',
      data: datapie,
      options: optionpie
    });


    var ctxLabel = grap1[0];
    var ctxData1 = grap1[1];
    var ctxColor1 = '#0d47a1';

    // Horizontal bar chart
    var ctx2 = document.getElementById('chartBar2').getContext('2d');
    new Chart(ctx2, {
      type: 'horizontalBar',
      data: {
        labels: ctxLabel,
        datasets: [{
          label: "Jumlah Pendapatan",
          data: ctxData1,
          backgroundColor: ctxColor1
        }]
      },
      options: {
        maintainAspectRatio: false,
        responsive: true,
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    var bilangan = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                    // console.log(bilangan);
                    var	reverse = bilangan.toString().split('').reverse().join(''),
                    	  ribuan 	= reverse.match(/\d{1,3}/g);
                    	  ribuan	= ribuan.join('.').split('').reverse().join('');
                    return "Rp "+ribuan;
                }
            }
        },
        legend: {
          display: true,
          labels: {
            display: true
          }
        },
        scales: {
          yAxes: [{
            gridLines: {
              display: false
            },
            ticks: {
              beginAtZero:true,
              fontSize: 10,
              fontColor: '#182b49'
            }
          }],
          xAxes: [{
            gridLines: {
              color: '#e5e9f2'
            },
            barPercentage: 1,
            ticks: {
              beginAtZero:true,
              fontSize: 11,
              fontColor: '#182b49'
            }
          }]
        }
      }
    });


  })
