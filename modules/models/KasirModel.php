<?php
/**
 * @Author  : Sofian Eka Sandra<fianeka.me@gmail.com>
 * @Date    : 12/05/17 - 3:32 AM
 */

class KasirModel extends Model{
    protected $tableName = "smis_ksr_bayar";
    public function get($params = "") {
        $data = array();
        $ksr = $this->db->getAll($this->tableName)->toObject();
        foreach($ksr as $val) {
            $total = $this->db->getWhere('smis_ksr_bayar', array('id' => $val->id))->numRows();
            $val->total = $total;
            array_push($data, $val);
        }
        return $data;
    }
}
?>
