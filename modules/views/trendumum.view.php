<!-- <pre>
<?php
// print_r($data["total"][0]->min);
// print_r($data["total"]);
?>
</pre> -->
<div class="d-sm-flex align-items-center justify-content-between mg-b-10 mg-lg-b-10">
  <div>
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb breadcrumb-style1 mg-b-10">
        <li class="breadcrumb-item"><a href="#">Trend Pendapatan</a></li>
        <li class="breadcrumb-item active" aria-current="page">Umum</li>
      </ol>
    </nav>
    <h4 class="mg-b-0 tx-spacing--1">Laporan Trend Pendapatan Secara Umum</h4>
  </div>

</div>

<div class="row row-xs">
  <div class="col-12">
    <div class="card card-body">
      <div class="d-md-flex align-items-center justify-content-between">
        <div class="media align-sm-items-center">
          <div class="media-body mg-l-15">
            <h6 class="tx-12 tx-lg-14 tx-semibold tx-uppercase tx-spacing-1 mg-b-5">Pendapatan Total<span class="tx-normal tx-color-03">(RP)</span></h6>
            <h6 class="tx-9  tx-lg-11 tx-semibold tx-uppercase tx-spacing-1 mg-b-5">PERIODE : <b class="tx-danger"><?php echo tanggal_indo($data["total"]->min,true); ?> - <?php echo tanggal_indo($data["total"]->max,true); ?></b></h6>
            <hr>
            <div class="d-flex align-items-baseline">
              <h2 class="tx-20 tx-lg-28 tx-normal tx-rubik lh-2 mg-b-0"><?php echo rupiah($data["total"]->total); ?></h2>
            </div>
            <div class="d-flex align-items-baseline">
              <h6 class="tx-11 tx-lg-12 tx-normal tx-rubik lh-2 mg-b-0">(<?php echo terbilang($data["total"]->total); ?> Rupiah)</h6>
            </div>
          </div><!-- media-body -->
        </div><!-- media -->
      </div>
    </div>
  </div><!-- col -->
  <div class="col-md-12 col-lg-12 col-xl-12 mg-t-10">
    <?php
      $grapdata = Array();
      $ruang = Array();
      $jml = Array();
      foreach ($data["grap1"] as $val) {
        $nama = $val->ruangan !== "" ? $val->ruangan : 'Undefined';
        array_push($ruang,ucwords(str_replace('_', ' ', $nama)));
        array_push($jml,round($val->jml,0));
      }
      array_push($grapdata, $ruang, $jml);
    ?>
    <script type="text/javascript">
      var grap1 = <?php echo json_encode($grapdata); ?>;
    </script>
    <div class="card">
      <div class="card-header">
        <h6 class="mg-b-0">Laporan Grafik : Trend Penghasilan Keseluruhan Dari Tiap Ruangan</h6>
      </div><!-- card-header -->
      <div class="card-body pd-lg-25">
        <!-- <div id="scrolly"> -->
          <div class="">
            <canvas id="chartBar2" height="2000px"></canvas>
          </div>
        <!-- </div> -->
        </div>
      </div><!-- card-body -->
      <div class="card-footer pd-20">
      </div><!-- card-footer -->
    </div><!-- card -->
  <div class="col-12 mg-t-0 mg-b-20">
    <div class="card">
      <div class="card-header">
        <h6 class="mg-b-0">Detail Pendapatan Tiap Ruangan</h6>
      </div><!-- card-header -->
      <div class="card-body pd-0">
        <div id="scrolly">
          <div class="">
            <div class="row no-gutters">
              <?php foreach ($data["grap1"] as $val):
                $nama = $val->ruangan !== "" ? $val->ruangan : 'Undefined';
                $ruang = ucwords(str_replace('_', ' ', $nama));
                $pendapatan = rupiah($val->jml);
              ?>
              <div class="col col-sm-6 col-lg-4">
                <div class="crypto">
                  <div class="media mg-b-10">
                    <div class="avatar"><span class="avatar-initial rounded-circle"><?php echo $ruang[0] . $ruang[1]; ?></span></div>
                    <div class="media-body pd-l-8">
                      <h6 class="tx-11 tx-spacing-1 tx-danger tx-uppercase tx-semibold mg-b-5"><?php echo $ruang; ?></h6>
                      <div class="d-flex align-items-baseline tx-rubik">
                        <h5 class="tx-20 mg-b-0"><?php echo $pendapatan; ?></h5>
                      </div>
                    </div><!-- media-body -->
                  </div><!-- media -->
                </div><!-- crypto -->
              </div>
              <?php endforeach; ?>
            </div><!-- row -->
          </div>
        </div>

      </div><!-- card-body -->
    </div><!-- card -->
  </div><!-- col -->
  <div class="col-md-6 col-lg-4 col-xl-4 mg-t-10">
    <div class="card">
      <div class="card-header">
        <h6 class="mg-b-0">Tipe Perawatan (Jumlah Dan Persentase)</h6>
      </div><!-- card-header -->
      <div class="card-body pd-lg-25">
        <div class="chart-seven"><canvas id="chartUrji"></canvas></div>
      </div><!-- card-body -->
      <div class="card-footer pd-20">
        <div class="row">
          <?php
            $totaldata = array_sum(array_column($data["grap2"],'jml'));
            $dataurji = Array();
            $ruang = Array();
            $jml = Array();
            $color = Array();
            foreach ($data["grap2"] as $val) {
              $randcolor = rand_color();
              $nama = $val->urji !== "" ? $val->urji : 'Undefined';
              $persen = ($val->jml/$totaldata)*100;
              array_push($ruang,$nama);
              array_push($jml,round($persen,2));
              array_push($color,$randcolor);
          ?>
          <div class="col-6 mg-b-20">
            <p class="tx-10 tx-uppercase tx-medium tx-color-03 tx-spacing-1 tx-nowrap mg-b-5"><?php echo $nama; ?></p>
            <div class="d-flex align-items-center">
              <div class="wd-10 ht-10 rounded-circle mg-r-5" style="background-color: <?php echo $randcolor; ?>;"></div>
              <h6 class="tx-normal tx-rubik mg-b-0"><?php echo $val->jml; ?><small class="tx-danger tx-color-02"> <?php echo round($persen,2); ?>%</small></h6>
            </div>
          </div><!-- col -->
          <?php
          }
            array_push($dataurji, $ruang, $jml, $color);
          ?>
          <script type="text/javascript">
            var grap2 = <?php echo json_encode($dataurji); ?>;
          </script>
        </div><!-- row -->
        <hr>
        <p class="tx-10 tx-uppercase tx-medium tx-color-02 tx-spacing-1 tx-nowrap mg-b-5">Jumlah Data = <?php echo $totaldata; ?></p>
      </div><!-- card-footer -->
    </div><!-- card -->
  </div>
  <div class="col-md-6 col-lg-4 col-xl-4 mg-t-10">
    <div class="card">
      <div class="card-header">
        <h6 class="mg-b-0">Metode Pembayaran (Jumlah Dan Persentase)</h6>
      </div><!-- card-header -->
      <div class="card-body pd-lg-25">
        <div class="chart-seven"><canvas id="chartMetod"></canvas></div>
      </div><!-- card-body -->
      <div class="card-footer pd-20">
        <div class="row">
          <?php
            $totaldata = array_sum(array_column($data["grap3"],'jml'));
            $datametod = Array();
            $ruang = Array();
            $jml = Array();
            $color = Array();
            foreach ($data["grap3"] as $val) {
              $randcolor = rand_color();
              $nama = $val->metode !== "" ? $val->metode : 'Undefined';
              $persen = ($val->jml/$totaldata)*100;
              array_push($ruang,$nama);
              array_push($jml,round($persen,2));
              array_push($color,$randcolor);
          ?>
          <div class="col-6 mg-b-20">
            <p class="tx-10 tx-uppercase tx-medium tx-color-03 tx-spacing-1 tx-nowrap mg-b-5"><?php echo $nama; ?></p>
            <div class="d-flex align-items-center">
              <div class="wd-10 ht-10 rounded-circle mg-r-5" style="background-color: <?php echo $randcolor; ?>;"></div>
              <h6 class="tx-normal tx-rubik mg-b-0"><?php echo $val->jml; ?><small class="tx-danger tx-color-02"> <?php echo round($persen,2); ?>%</small></h6>
            </div>
          </div><!-- col -->
          <?php
          }
            array_push($datametod, $ruang, $jml, $color);
          ?>
          <script type="text/javascript">
            var grap3 = <?php echo json_encode($datametod); ?>;
          </script>
        </div><!-- row -->
        <hr>
        <p class="tx-10 tx-uppercase tx-medium tx-color-02 tx-spacing-1 tx-nowrap mg-b-5">Jumlah Data = <?php echo $totaldata; ?></p>
      </div><!-- card-footer -->
    </div><!-- card -->
  </div>
  <div class="col-md-6 col-lg-4 col-xl-4 mg-t-10">
    <div class="card">
      <div class="card-header">
        <h6 class="mg-b-0">Cara Pembayaran (Jumlah Dan Persentase)</h6>
      </div><!-- card-header -->
      <div class="card-body pd-lg-25">
        <div class="chart-seven"><canvas id="chartCara"></canvas></div>
      </div><!-- card-body -->
      <div class="card-footer pd-20">
        <div class="row">
          <?php
            $totaldata = array_sum(array_column($data["grap4"],'jml'));
            $databayar = Array();
            $ruang = Array();
            $jml = Array();
            $color = Array();
            foreach ($data["grap4"] as $val) {
              $randcolor = rand_color();
              $nama = $val->carabayar !== "" ? $val->carabayar : 'Undefined';
              $persen = ($val->jml/$totaldata)*100;
              array_push($ruang,$nama);
              array_push($jml,round($persen,2));
              array_push($color,$randcolor);
          ?>
          <div class="col-6 mg-b-20">
            <p class="tx-10 tx-uppercase tx-medium tx-color-03 tx-spacing-1 tx-nowrap mg-b-5"><?php echo $nama; ?></p>
            <div class="d-flex align-items-center">
              <div class="wd-10 ht-10 rounded-circle mg-r-5" style="background-color: <?php echo $randcolor; ?>;"></div>
              <h6 class="tx-normal tx-rubik mg-b-0"><?php echo $val->jml; ?><small class="tx-danger tx-color-02"> <?php echo round($persen,2); ?>%</small></h6>
            </div>
          </div><!-- col -->
          <?php
          }
            array_push($databayar, $ruang, $jml, $color);
          ?>
          <script type="text/javascript">
            var grap4 = <?php echo json_encode($databayar); ?>;
          </script>
        </div><!-- row -->
        <hr>
        <p class="tx-10 tx-uppercase tx-medium tx-color-02 tx-spacing-1 tx-nowrap mg-b-5">Jumlah Data = <?php echo $totaldata; ?></p>
      </div><!-- card-footer -->
    </div><!-- card -->
  </div>


</div><!-- row -->

<script src="resource/assets/js/charset.js"></script>
