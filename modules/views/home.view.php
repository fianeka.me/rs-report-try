<div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-30">
  <div>
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb breadcrumb-style1 mg-b-10">
        <li class="breadcrumb-item"><a href="#">Syamrabu Dashboard Report</a></li>
      </ol>
    </nav>
    <h4 class="mg-b-0 tx-spacing--1"></h4>
  </div>
</div>
