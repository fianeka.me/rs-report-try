<div class="d-sm-flex align-items-center justify-content-between mg-b-10 mg-lg-b-10">
  <div>
    <h4 class="mg-b-0 tx-spacing--1">Penerimaan Kasir RSUD Syamrabu</h4>
  </div>
</div>
<hr>
<div class="row row-xs">
  <div class="col-12">
    <form method="post">
      <input type="hidden" id="tgl" name="tgl" value="">
      <input type="hidden" id="periode" name="periode" value="">
      <div class="form-row">
         <div class="form-group col-md-6">
           <label for="inputEmail4">Pilih Periode Tanggal</label>
           <input type="text" class="form-control" id="reportrange">
         </div>
         <div class="form-group col-md-6">
           <label for="inputPassword4">Pilih Jenis Pembayaran</label>
           <select name="jenis" class="form-control select2">
            <option value="all">Semua Jenis Pembayaran</option>
            <?php
              foreach ($data["jenisbayar"] as $val) {
                $nama = $val->carabayar !== "" ? $val->carabayar : 'Undefined';
                $nama = ucwords(str_replace('_', ' ', $nama));
            ?>
              <option value=<?php echo $val->carabayar ?>><?php echo $nama ?></option>
            <?php
              }
            ?>
           </select>
         </div>
      </div>
      <button type="submit" class="btn btn-sm btn-block btn-primary">Lihat Trend Pendapatan</button>
    </form>
    <hr>
  </div><!-- col -->
  <div class="col-12">
    <?php if (isset($data["laporan"])): ?>
      <?php
        if ($data["period"][0] == $data["period"][1]) {
          $period = tanggal_indo($data["period"][0],true);
        }else{
          $period = tanggal_indo($data["period"][0],true) ." - ". tanggal_indo($data["period"][1],true) ;
        }
      ?>
      <?php if (count($data["laporan"]) > 0): ?>
        <?php
          $jmluang = array_sum(array_column($data["laporan"],'jml'));
          $grapdata = Array();
          $ruang = Array();
          $jml = Array();
          foreach ($data["laporan"] as $val) {
            $nama = $val->ruangan !== "" ? $val->ruangan : 'Undefined';
            array_push($ruang,ucwords(str_replace('_', ' ', $nama)));
            array_push($jml,round($val->jml,0));
          }
          array_push($grapdata, $ruang, $jml);
        ?>
        <script type="text/javascript">
          var grap1 = <?php echo json_encode($grapdata); ?>;
        </script>
        <div class="card">
          <div class="card-header">
            <h5 class="mg-b-0">Laporan Grafik : Penerimaan Kasir RSUD Syamrabu Dari Tiap Ruangan Di Periode :</h5>
            <h6 class="mg-b-0"><b> <?php echo $period; ?></b></h6>
            <?php
              $jenisnya =  $data["carabayar"] !== "all" ? ucwords(str_replace('_', ' ', $data["carabayar"])) : 'Seluruh Jenis Pembayaran';
            ?>
            <h6 class="mg-b-0">Jenis Pembayaran :<b> <?php echo $jenisnya; ?></b></h6>
          </div><!-- card-header -->
          <div class="card-body pd-lg-25">
            <!-- <div id="scrolly"> -->
              <div class="">
                <canvas id="chartBar" height="1500px"></canvas>
              </div>
            <!-- </div> -->
          </div>
        </div><!-- card-body -->
          <div class="card mg-t-5 ">
            <div class="card-header">
              <h6 class="mg-b-0">Detail Pendapatan Tiap Ruangan</h6>
            </div><!-- card-header -->
            <div class="card-body pd-0">
              <div id="scrolly">
                <div class="">
                  <div class="row no-gutters">
                    <?php foreach ($data["laporan"] as $val):
                      $nama = $val->ruangan !== "" ? $val->ruangan : 'Undefined';
                      $ruang = ucwords(str_replace('_', ' ', $nama));
                      $pendapatan = rupiah($val->jml);
                    ?>
                    <div class="col col-sm-6 col-lg-4">
                      <div class="crypto">
                        <div class="media mg-b-10">
                          <div class="avatar"><span class="avatar-initial rounded-circle"><?php echo $ruang[0] . $ruang[1]; ?></span></div>
                          <div class="media-body pd-l-8">
                            <h6 class="tx-11 tx-spacing-1 tx-danger tx-uppercase tx-semibold mg-b-5"><?php echo $ruang; ?></h6>
                            <div class="d-flex align-items-baseline tx-rubik">
                              <h5 class="tx-20 mg-b-0"><?php echo $pendapatan; ?></h5>
                            </div>
                          </div><!-- media-body -->
                        </div><!-- media -->
                      </div><!-- crypto -->
                    </div>
                    <?php endforeach; ?>
                  </div><!-- row -->
                </div>
              </div>
            </div><!-- card-body -->
            <div class="card-header">
              <hr>
              <h5 class="mg-b-0">Total Pendapatan :<b> <?php echo rupiah($jmluang,true); ?></b></h5>
              <h6 class="tx-11 tx-lg-12 tx-normal tx-rubik lh-2 mg-b-0">(<?php echo terbilang($jmluang); ?> Rupiah)</h6>

            </div><!-- card-header -->
          </div><!-- card -->
      <?php else: ?>
        <div class="alert alert-danger d-flex align-items-center" role="alert">
          <i data-feather="alert-circle" class="mg-r-10"></i>
          Tidak Ada Laporan Pendapatan Di Periode : <b> <?php echo $period; ?></b>
        </div>
      <?php endif; ?>

    <?php else: ?>
      <!-- <div class="alert alert-warning d-flex align-items-center" role="alert">
        <i data-feather="alert-circle" class="mg-r-10"></i> Tanggal Belum Terisi
      </div> -->
    <?php endif; ?>
  </div>
</div><!-- row -->


<script type="text/javascript">
$(function() {
    // var start = moment(orders.order_sdate, "YYYY-MM-DD" );
    // var end = moment( orders.order_edate, "YYYY-MM-DD" );
    var start = moment().startOf('month');
    var end = moment().endOf('month');
    console.log(start);
    function cb(start, end) {
      document.getElementById("tgl").value = start.format('Y-MM-DD') + '|' + moment(end).add(1, 'days').format('Y-MM-DD');
      document.getElementById("periode").value = start.format('Y-MM-DD') + '|' + end.format('Y-MM-DD');
      $('#reportrange span').html(start.format('D MMMM YYYY') + ' - ' + end.format('D MMMM YYYY'));
    }
    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Hari Ini': [moment(), moment()],
           'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           '7 Hari Terhakir': [moment().subtract(6, 'days'), moment()],
           '30 Hari Terhakir': [moment().subtract(29, 'days'), moment()],
           'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
           'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        locale: {
          "customRangeLabel": "Pilih Tanggal Lain",
        },
    }, cb);
    cb(start, end);
});
</script>

<script type="text/javascript">
$('.select2').select2({
  placeholder: 'Jenis Pembayaran',
  searchInputPlaceholder: 'Search options'
});

var ctxLabel = grap1[0];
var ctxData1 = grap1[1];
var ctxColor1 = '#0d47a1';

// Horizontal bar chart
var ctx2 = document.getElementById('chartBar').getContext('2d');
new Chart(ctx2, {
  type: 'horizontalBar',
  data: {
    labels: ctxLabel,
    datasets: [{
      label: "Pendapatan",
      data: ctxData1,
      backgroundColor: ctxColor1
    }]
  },
  options: {
    maintainAspectRatio: false,
    responsive: true,
    tooltips: {
        callbacks: {
            label: function(tooltipItem, data) {
                var bilangan = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                // console.log(bilangan);
                var	reverse = bilangan.toString().split('').reverse().join(''),
                    ribuan 	= reverse.match(/\d{1,3}/g);
                    ribuan	= ribuan.join('.').split('').reverse().join('');
                return "Rp "+ribuan;
            }
        }
    },
    legend: {
      display: false,
      labels: {
        display: false
      }
    },
    scales: {
      yAxes: [{
        gridLines: {
          display: false
        },
        ticks: {
          beginAtZero:true,
          fontSize: 10,
          fontColor: '#182b49'
        }
      }],
      xAxes: [{
        gridLines: {
          color: '#e5e9f2'
        },
        barPercentage: 0.6,
        ticks: {
          beginAtZero:true,
          fontSize: 11,
          fontColor: '#182b49'
        }
      }]
    }
  }
});
</script>
