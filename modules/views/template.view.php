<!DOCTYPE html>
<html lang="en">
  <head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="Responsive Bootstrap 4 Dashboard Template">
    <meta name="author" content="ThemePixels">

    <!-- Favicon -->

    <title>Syamrabu Report Apps</title>


    <!-- DashForge CSS -->
    <link rel="stylesheet" href="resource/lib/select2/css/select2.min.css" rel="stylesheet">
    <link rel="stylesheet" href="resource/assets/css/dashforge.css">
    <link rel="stylesheet" href="resource/assets/css/custom.css">
    <link rel="stylesheet" href="resource/assets/css/dashforge.dashboard.css">

    <!-- vendor css -->
    <link href="resource/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="resource/lib/datepickerfull/daterangepicker.css" rel="stylesheet" />


    <script src="resource/lib/jquery/jquery.min.js"></script>
    <script src="resource/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="resource/lib/feather-icons/feather.min.js"></script>
    <script src="resource/lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>

    <script src="resource/lib/moment/moment.js"></script>
    <script src="resource/lib/datepickerfull/daterangepicker.js"></script>
    <script src="resource/lib/chart.js/Chart.bundle.min.js"></script>
    <script src="resource/lib/select2/js/select2.min.js"></script>


    <script src="resource/assets/js/dashforge.js"></script>
    <script src="resource/assets/js/dashforge.aside.js"></script>
    <script src="resource/assets/js/dashforge.sampledata.js"></script>

    <!-- append theme customizer -->
    <script src="resource/lib/js-cookie/js.cookie.js"></script>
    <script src="resource/assets/js/dashforge.settings.js"></script>

  </head>
  <body>
    <!-- <aside class="aside aside-fixed">
      <div class="aside-header">
        <a class="aside-logo">syamrabu<span>Apps</span></a>
        <a href="" class="aside-menu-link">
          <i data-feather="menu"></i>
          <i data-feather="x"></i>
        </a>
      </div>
      <div class="aside-body">
        <ul class="nav nav-aside">
          <li class="nav-label">Trend Pendapatan</li>
          <li class="nav-item"><a href="<?php echo PATH; ?>?page=kasir" class="nav-link"><i data-feather="home"></i> <span>Laporan Kasir</span></a></li>
          <li class="nav-item"><a href="<?php echo PATH; ?>?page=kasir&&action=bydate" class="nav-link"><i data-feather="calendar"></i> <span>Berdasarkan Waktu</span></a></li>
          <li class="nav-item"><a href="" class="nav-link"><i data-feather="loader"></i> <span>Berdasarkan Ruangan</span></a></li>

          <li class="nav-label mg-t-25">Laporan PIP</li>
          <li class="nav-item"><a href="" class="nav-link"><i data-feather="loader"></i> <span>Menu 1</span></a></li>
          <li class="nav-item"><a href="" class="nav-link"><i data-feather="loader"></i> <span>Menu 2</span></a></li>

          <li class="nav-label mg-t-25">Laporan PMKP</li>
          <li class="nav-item"><a href="" class="nav-link"><i data-feather="loader"></i> <span>Menu 1</span></a></li>
          <li class="nav-item"><a href="" class="nav-link"><i data-feather="loader"></i> <span>Menu 2</span></a></li>

        </ul>
      </div>
    </aside> -->
    <div class="content ht-100v pd-0">
      <!-- <div class="content-header">
      </div> -->
      <div class="content-body">
        <div class="container pd-x-0">
          <?php
              $view = new View($viewName);
              $view->bind('data', $data);
              $view->forceRender();
          ?>
        </div><!-- container -->
      </div>
    </div><!-- content -->
  </body>
</html>
