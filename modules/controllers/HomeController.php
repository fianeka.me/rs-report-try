<?php

use \modules\controllers\MainController;

class HomeController extends MainController {

    public function index() {
      $id = isset($_GET["id"]) ? $_GET["id"] : "";
      $this->model('kasir');
      $carabayar = $this->kasir->getDistinct("carabayar");
      if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $tgl     = isset($_POST["tgl"])? $_POST["tgl"]   : "";
        $jenis  = isset($_POST["jenis"])? $_POST["jenis"]   : "all";
        $period     = isset($_POST["periode"])? $_POST["periode"]   : "";
        $newdate = explode("|",$tgl);
        $period = explode("|",$period);
        $start = $newdate[0];
        $end = $newdate[1];
        if ($jenis=="all") {
          $datagraf = $this->kasir->getCustom(
            "ruangan, sum(nilai) as jml ",
            "WHERE ruangan is not null AND waktu >= '".$start."' AND waktu < '".$end."' GROUP BY ruangan"
          );
        }else{
          $datagraf = $this->kasir->getCustom(
            "ruangan, sum(nilai) as jml ",
            "WHERE ruangan is not null AND waktu >= '".$start."' AND waktu < '".$end."' AND carabayar='".$jenis."' GROUP BY ruangan"
          );
        }
        $this->template('trenddate', array('jenisbayar' => $carabayar, 'laporan' => $datagraf, "period" => $period, "carabayar" => $jenis));

      }else{
        $jenis = "all";
        $start = date('Y-m-01');
        $end =  date('Y-m-t');
        $datagraf = $this->kasir->getCustom(
          "ruangan, sum(nilai) as jml ",
          "WHERE ruangan is not null AND waktu >= '".$start."' AND waktu < '".$end."' GROUP BY ruangan"
        );
        $this->template('trenddate', array('jenisbayar' => $carabayar, 'laporan' => $datagraf, "period" => Array($start,$end), "carabayar" => $jenis));
      }
    }
}
?>
