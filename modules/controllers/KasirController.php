<?php

use \modules\controllers\MainController;

class KasirController extends MainController {

  public function index() {
    $id = isset($_GET["id"]) ? $_GET["id"] : "";
    $this->model('kasir');
    $carabayar = $this->kasir->getDistinct("carabayar");
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $tgl     = isset($_POST["tgl"])? $_POST["tgl"]   : "";
      $jenis  = isset($_POST["jenis"])? $_POST["jenis"]   : "all";
      $period     = isset($_POST["periode"])? $_POST["periode"]   : "";
      $newdate = explode("|",$tgl);
      $period = explode("|",$period);
      $start = $newdate[0];
      $end = $newdate[1];
      if ($jenis=="all") {
        $datagraf = $this->kasir->getCustom(
          "ruangan, sum(nilai) as jml ",
          "WHERE ruangan is not null AND waktu >= '".$start."' AND waktu < '".$end."' GROUP BY ruangan"
        );
      }else{
        $datagraf = $this->kasir->getCustom(
          "ruangan, sum(nilai) as jml ",
          "WHERE ruangan is not null AND waktu >= '".$start."' AND waktu < '".$end."' AND carabayar='".$jenis."' GROUP BY ruangan"
        );
      }
      $this->template('trenddate', array('jenisbayar' => $carabayar, 'laporan' => $datagraf, "period" => $period, "carabayar" => $jenis));

    }else{
      $jenis = "all";
      $start = date('Y-m-01');
      $end =  date('Y-m-t');
      $datagraf = $this->kasir->getCustom(
        "ruangan, sum(nilai) as jml ",
        "WHERE ruangan is not null AND waktu >= '".$start."' AND waktu < '".$end."' GROUP BY ruangan"
      );
      $this->template('trenddate', array('jenisbayar' => $carabayar, 'laporan' => $datagraf, "period" => Array($start,$end), "carabayar" => $jenis));
    }
  }

  public function bydate() {
    $this->model('kasir');
    $datatotal = $this->kasir->getCustom(
      "MIN(waktu) as min , MAX(waktu) as max, sum(nilai) as total"
    );
    $datagraf = $this->kasir->getCustom(
      "ruangan, sum(nilai) as jml ",
      "WHERE ruangan is not null GROUP BY ruangan"
    );
    $dataurji = $this->kasir->getCustom(
      "urji, count(*) as jml ",
      "WHERE urji is not null GROUP BY urji"
    );
    $datametod = $this->kasir->getCustom(
      "metode, count(*) as jml ",
      "WHERE metode is not null GROUP BY metode"
    );
    $databayar = $this->kasir->getCustom(
      "carabayar, count(*) as jml ",
      "WHERE carabayar is not null GROUP BY carabayar"
    );
    $this->template('trendumum', array('total' => $datatotal[0],'grap1' => $datagraf,'grap2' => $dataurji,'grap3' => $datametod,'grap4' => $databayar));
  }

  public function delete() {

  }

  public function insert() {

  }

  public function update() {

  }
}
?>
